import pytest
from mcexport.extractor import arguments_combinations

def test_creating_combinations_of_arugments_all_three():
    config = {
        'list_id': 'foo',
        'status': ['cleaned', 'unsubscribed'],
        'segments': ['first', 'second']
    }
    combos = list(arguments_combinations(config))
    expected_combinations = [
        ('foo', 'cleaned', 'first'),
        ('foo', 'cleaned', 'second'),
        ('foo', 'unsubscribed', 'first'),
        ('foo', 'unsubscribed', 'second')
    ]
    for combo in expected_combinations:
        assert combo in combos
    assert len(combos) == 4

def test_creating_combionations_of_args_wo_segments():
    config = {
        'list_id': 'foo',
        'status': ['cleaned', 'unsubscribed']
    }
    combos = list(arguments_combinations(config))
    expected_combinations = [
        ('foo', 'cleaned', None),
        ('foo', 'unsubscribed', None),
    ]
    for combo in expected_combinations:
        assert combo in combos
    assert len(combos) == 2
