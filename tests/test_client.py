import os
import pytest
from mcexport.client import ExportClient
from mcexport.extractor import main
import glob
import csv

def _make_client():
    return ExportClient(os.getenv("EX_MC_APIKEY"))
TEST_CAMPAIGN_ID = os.getenv("EX_MC_CAMPAIGN_ID")

@pytest.fixture
def client():
    return _make_client()

def test_instantiating_client():
    cl = _make_client()
    assert cl.dc == os.getenv("EX_MC_APIKEY").split('-')[1]

def _validate_output(exported_csv):
    assert os.stat(exported_csv).st_size > 0
    with open(exported_csv, 'r') as f:
        reader= csv.reader(f)
        first_row = next(reader)
        assert first_row[0] == 'list_id'

def test_downloading_subscriber_list(client, tmpdir):
    outdir = tmpdir.mkdir('output')
    outpath = os.path.join(outdir.strpath, 'exported_list.csv')

    list_id = os.getenv("EX_MC_LIST_ID")
    exported_csv, columns = client.list_export(
        list_id=list_id,
        status='subscribed',
        write_header=True,
        outpath=outpath)
    _validate_output(exported_csv)

def test_downloading_client_through_config():
    list_id = os.getenv("EX_MC_LIST_ID")
    status = 'subscribed'

    config = {
        "bucketname" : "in.c-ex-mailchimp-export",
        "#apikey": os.getenv("EX_MC_APIKEY"),
        "export": {
            "lists": [
                {
                    "list_id": list_id,
                    "status": ["subscribed", "cleaned"],
                    "segments": []
                }
            ],
            "since": "",
            "subscriber_activity": {
                "include_empty": False,
                "list_ids": [list_id],
                "campaign_ids": [TEST_CAMPAIGN_ID]
      }
        }
    }

    main(config)
    expected_output_path_sub = os.path.join(
        os.getenv("KBC_DATADIR"),
        'out/tables',
        '{list_id}_{status}_{segment}.csv'.format(
            list_id=list_id,
            status='subscribed',
            segment=None
        )
    )
    expected_output_path_activity = os.path.join(
        os.getenv("KBC_DATADIR"),
        'out/tables',
        'subscriber_activity.csv'
    )
    assert os.path.isfile(expected_output_path_activity)
    assert os.stat(expected_output_path_activity).st_size > 1
    expected_output_path_clean = os.path.join(
        os.getenv("KBC_DATADIR"),
        'out/tables',
        '{list_id}_{status}_{segment}.csv'.format(
            list_id=list_id,
            status='cleaned',
            segment=None
        )
    )
    _validate_output(expected_output_path_sub)
    assert os.path.isfile(expected_output_path_sub)
    assert os.path.isfile(expected_output_path_clean)

def test_downloading_subscriber_activity(tmpdir, client):
    outdir = tmpdir.mkdir('output')
    outpath = os.path.join(outdir.strpath, 'sub_activity.csv')

    exported_csv = client.subscriber_activity(
        campaign_id=TEST_CAMPAIGN_ID,
        outpath=outpath,
        write_header=True)

    with open(outpath, 'r') as f:
        reader= csv.DictReader(f)
        first_row = next(reader)
        assert '@' in first_row['email_address']
