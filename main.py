import os
import sys
import traceback
from mcexport.extractor import parse_cfg, main, update_statefile
from mcexport.client import ApiError
from requests import RequestException

if __name__ == '__main__':
    try:
        config = parse_cfg(os.getenv("KBC_DATADIR"))
        started_at = main(config)
        # save statefile with started_at
        new_state = {'last_run': str(started_at)}
        update_statefile(new_state)
    except (ValueError, KeyError, ApiError, RequestException) as e:
        traceback.print_exc(file=sys.stderr)
        sys.exit(1)
    except:
        traceback.print_exc(file=sys.stderr)
        sys.exit(2)
