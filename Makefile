IMAGE=pocin/kbc-ex-mailchimp-export-api
MEIRO_IMAGE=docker-registry.meiro.io/components/mailchimp_connector
VERSION=v0.8.2
BASECOMMAND=docker run -it --rm -v `pwd`:/src -e KBC_DATADIR='/data/' $(IMAGE):latest

# run a production grade container for inspection
prod:
	docker run -it --rm $(IMAGE):latest /bin/ash

build-dev:
	docker build . -t $(IMAGE):dev

build:
	docker build . -t $(IMAGE):$(VERSION) -t $(IMAGE):latest

build-meiro:
	docker build . -t $(MEIRO_IMAGE):$(VERSION) -t $(MEIRO_IMAGE):latest
test:
	$(BASECOMMAND) ./run_tests.sh

sh:
	$(BASECOMMAND) /bin/ash

run:
	$(BASECOMMAND)

deploy-meiro:
	echo "Pushing to meiro docker registry"
	docker push $(MEIRO_IMAGE):latest
	docker push $(MEIRO_IMAGE):$(VERSION)
deploy:
	echo "Pushing to dockerhub"
	docker push $(IMAGE):$(VERSION)
	docker push $(IMAGE):latest
