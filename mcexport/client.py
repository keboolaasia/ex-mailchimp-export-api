import requests
import re
import csv
import json
import parsedatetime
import datetime
from itertools import groupby

class ApiError(Exception):
    pass

class ExportClient:
    valid_stati = {'subscribed', 'unsubscribed', 'cleaned'}
    def __init__(self, apikey):
        """

        Args:
            apikey (str): a valid API Key for your user account
        """
        self.apikey = apikey
        self.dc = apikey.split('-')[1]
        self.root_url = 'https://{dc}.api.mailchimp.com/export/1.0/'.format(
            dc=self.dc)

    def list_export(self, list_id, status, outpath,
                    segment=None, since=None, hashed=None,
                    write_header=True):
        """
        Exports members of a list and all of their associated details. This is a
        very similar to exporting via the web interface.

        Args:
            id: the list id to get members from
            status: optional, the status to get members for one of
                (subscribed, unsubscribed, cleaned), defaults to subscribed
            segment: optional pull only a certain Segment of your list.
            since: optional only return member whose data has changed since a
                GMT timestamp in YYYY-MM-DD HH:mm:ss format
            hashed: optional if, instead of full list data, you’d prefer a
                hashed list of email addresses, set this to the hashing
                algorithm you expect. Currently only "sha256" is supported.
            outpath (str): /local/path/to/results.csv where the response will be saved as a csv
            write_header (bool): if True (default) writes csv with regular header. If False, writes the csv without header

        Returns:
            a tuple (outputpath, header)

        """
        params = {
            'id': list_id,
            'apikey': self.apikey
        }
        if status not in self.valid_stati:
            raise ValueError("status must be one of {}, not '{}'".format(self.valid_stati, status))
        else:
            params['status'] = status

        if segment:
            params['segment']= segment
        if since:
            params['since'] = str(since)
        if hashed:
            if hashed != 'sha256':
                raise ValueError("Only support sha256 hasing, not '{}'".format(hashed))
            params['hashed'] = hashed
        print("Fetching {} {} {}...".format(list_id, status, segment))
        resp = requests.get(self.root_url + 'list/', params=params, stream=True)
        resp.raise_for_status()
        print("Downloading to {}".format(outpath))

        enrichment_header = ["list_id", "status", "segment"]
        enrichment_values = [list_id, status, segment]
        with open(outpath, 'w') as f:
            writer = csv.writer(f)
            lines = resp.iter_lines(decode_unicode=True)
            # the header doesnt contain metadata on the list_id and the
            # subscription status we have to enrich it
            try:
                _header_raw = next(lines)
                msg = "{}; {}".format(_header_raw, "this could be an invalid api key or wrong list id")
                _header = json.loads(_header_raw)
            except TypeError:
                # Despite it's 200 response, the response is an error json
                raise ApiError(msg)
            else:
                if isinstance(_header, dict):
                    raise ApiError(msg)
                header = enrichment_header + _header
                if write_header:
                    writer.writerow(header)
                for line in lines:
                    if line:
                        line_decoded = json.loads(line)
                        writer.writerow(enrichment_values + line_decoded)

        print("Report saved")
        return outpath, header


    def subscriber_activity(self, campaign_id, outpath, since=None, include_empty=False, write_header=True):
        '''
        Exports all Subscriber Activity for the requested campaign.

        This is a
        very similar to exporting via the web interface.

        campaignSubscriberActivity
        Args:
            campaign_id: the campaign_id
            since: optional only return member whose data has changed since a
                GMT timestamp in YYYY-MM-DD HH:mm:ss format or a human readable
                string
            ouptath (str): path/to/output.csv where the response will be saved

        Returns:
            a tuple (outpath, columns):
                outpath (str): path/to/output.csv (same as input arg)
                columns: the final columns in the file


        '''
        params = {
            'id': campaign_id,
            'apikey': self.apikey,
            'include_empty': bool(include_empty),
        }
        if since:
            params['since'] = str(since)
        print("Fetching list activity details for campaign {}...".format(campaign_id))
        resp = requests.get(self.root_url + 'campaignSubscriberActivity/',
                            params=params,
                            stream=True)
        resp.raise_for_status()
        header = ['campaign_id', 'email_address', 'timestamp', 'action', 'ip', 'url']
        enrichment_values = {'campaign_id': campaign_id}

        print("Downloading to '{}'".format(outpath))
        with open(outpath, 'w') as f:
            writer = csv.DictWriter(f, fieldnames=header)
            lines = resp.iter_lines(decode_unicode=True)
            if write_header:
                writer.writeheader()
            for line in lines:
                # we need to strip in case the line is just a whitespace,
                # we can skip this
                if line.strip():
                    try:
                        json_line = json.loads(line)
                    except (json.JSONDecodeError, TypeError) as e:
                        print("ERROR: line is not valid json, it's '{}'".format(line))
                        raise
                    else:
                        for line_decoded in self._serialize_subscriber_activity_line(json_line):
                            line_decoded.update(enrichment_values)
                            writer.writerow(line_decoded)

        return outpath

    @staticmethod
    def _serialize_subscriber_activity_line(line):
        """

        Args:
            line: in this format
        {'foo@bar.com': [
            {
              'action': 'open',
              'ip': '66.249.93.221',
              'timestamp': '2017-10-30 10:16:16',
              'url': None
            },
            {
              'action': 'click',
              'ip': '88.100.8.1',
              'timestamp': '2017-10-30 10:16:42',
              'url': 'https://wwwfoobar.com'},
            {
                'action': 'open',
                'ip': '88.100.8.1',
                'timestamp': '2017-10-30 10:16:42',
                'url': None
            }
          ]
        }

        Retruns:
            a generator, which yields a serialized line - an action + email address
                {'action': 'open',
                'email_address': 'xuz@foo.com',
                'ip': '66.249.81.92',
                'timestamp': '2017-10-30 10:07:05',
                'url': ''}
            in case there are no actions and include_empty=True, create a dummy line with 'action': "no_interaction"

        """

        if len(line) != 1:
            raise Exception("The api returned {}, make sure your campaign and list ids are valid!".format(line))
        for email, actions in list(line.items()):
            # ugly? hack to unpack the items
            # we know theat the line only contains one email and one dict of actions
            pass
        if not actions:
            yield {'action': 'no_interaction',
                   'email_address': email,
                   'ip': None,
                   'timestamp': None,
                   'url': None}
        else:
            serialized = {}
            for action in actions:
                action['email_address'] = email
                yield action
