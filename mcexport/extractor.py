"""
export
"""
import tarfile
import requests
from pathlib import Path
import time
from keboola import docker
from mailchimp3 import MailChimp
import json
import traceback
import csv
import os
import datetime
from collections import namedtuple
import sys
import itertools
import parsedatetime
from mcexport.client import ExportClient

# uses default bucket in kbc

BATCH_POLLING_DELAY = 10 #seconds

def parse_cfg(datadir):
    cfg = docker.Config(datadir)
    params = cfg.get_parameters()
    return params


def parse_since_ddt(since):
    calendar = parsedatetime.Calendar()
    dt, status = calendar.parse(since)
    if status == 0:
        raise ValueError(
            "Could not parse '{}' into YYYY-MM-DD HH:mm:ss"
            " GMT (!!!) format".format(since))
    else:
        ddt = datetime.datetime(*dt[:6])
        print("Parsed '{}' into {}".format(since, str(ddt)))
        return ddt


def main(config):
    # this is used to log the last time the ex was run
    until = datetime.datetime.now()
    client = ExportClient(config['#apikey'])

    custom_cfg = config.get('custom', {})
    received_campaigns_by_segmetn_cfg = custom_cfg.get('received_campaigns_by_segment')
    if received_campaigns_by_segmetn_cfg:
        batch_response = get_received_campaigns_for_segment(
            config['#apikey'],
            list_id=received_campaigns_by_segmetn_cfg['list_id'],
            segment_id=received_campaigns_by_segmetn_cfg['segment_id'],
            wait=True)
        serialize_received_campaigns_batch_results(batch_response, os.getenv("KBC_DATADIR"))

    export = config.get('export', {})
    _since= export.get('since')
    if _since == 'last':
        # load datetime from statefile
        state = parse_statefile('/data/in/state.json')
        since = state.get('last_run') # if not last run
        if since is None:
            print("no 'last_run' in statefile, downloading from the beginning")
    elif _since is not None and _since != '':
        since = parse_since_ddt(_since)
    else:
        since = None

    lists_config = export.get('lists', [])
    for list_config in lists_config:
        list_id = list_config['list_id']
        for arguments in arguments_combinations(list_config):
            fname = '{args.list_id}_{args.status}_{args.segment}.csv'.format(
                args=arguments)
            outpath = os.path.join(os.getenv("KBC_DATADIR"), 'out/tables', fname)
            outpath, columns = client.list_export(
                list_id=arguments.list_id,
                status=arguments.status,
                segment=arguments.segment,
                outpath=outpath,
                since=since,
                write_header=True)
    activity_config = export.get("subscriber_activity")
    if activity_config is not None:
        list_ids = activity_config.get('list_ids', [])
        mc_client = MailChimp(mc_api=config['#apikey'], mc_user='')
        # i don't like nested list comprehensions
        campaigns_since = since.isoformat() + '+00:00' if since is not None else since
        print("Downloading campaigns since", campaigns_since)

        what = set()
        if activity_config.get('include_automations', False):
            what.add('automations')
        if activity_config.get('include_campaigns', True):
            what.add('campaigns')


        list_of_campaigns = []
        for list_id in list_ids:
            _campaigns = download_all_campaign_ids(
                mc_client,
                list_id,
                what=what,
                since_send_time=campaigns_since)
            for campaign in _campaigns:
                list_of_campaigns.append(campaign)

        all_campaigns = list(set(list_of_campaigns + activity_config.get('campaign_ids', [])))

        include_empty = activity_config.get('include_empty', False)
        activity_final_outpath = download_subscriber_activity_for_list_of_campaigns(
            client,
            all_campaigns,
            since,
            include_empty)
    return until


def get_received_campaigns_for_segment(api_key, list_id, segment_id, wait=True):
    """If a member received automated email is only in this endpoint
    https://mailchimp.com/developer/reference/lists/list-members/list-member-activity/
    you can create a dummy segment to narrow down the subscriber ids you will be processing
    """

    client = MailChimp(mc_api=api_key, mc_user='')

    path_template = '/lists/{list_id}/members/{subscriber_hash}/activity'
    batches = []
    for sub in client.lists.segments.members.all(
            list_id, segment_id,
            get_all=True, fields='members.id',
            count=1000)['members']:
        # https://mailchimp.com/developer/reference/lists/list-members/list-member-activity/
        batch_request = {
            'method': 'GET',
            'path': path_template.format(list_id=list_id, subscriber_hash=sub['id']),
            # get just sent items, we can get the rest from regular endpoint
            'params': {'action': 'sent'}
        }
        batches.append(batch_request)

    batch_response = client.batches.create({"operations": batches})
    if wait:
        batch_response = wait_for_batch_to_finish(client, batch_response['id'])
    return batch_response


def serialize_received_campaigns_batch_results(batch_response, datadir):
    datadir = Path(datadir)
    temp_path = datadir / 'out/tables/recipients_by_segment.tar.gz'
    outpath = datadir / 'out/tables/recipients_by_segment.csv'
    print("Serializing {} to {}".format(batch_response, str(outpath)))

    with open(temp_path, 'wb') as outf:
        resp = requests.get(batch_response['response_body_url'], stream=True)
        for chunk in resp.iter_content(chunk_size=4096):
            outf.write(chunk)

    with tarfile.open(temp_path) as tar, outpath.open('w') as outf:
        writer = csv.DictWriter(
            outf,
            fieldnames=['list_id', 'email_id', 'action',
                        'timestamp','type', 'campaign_id', 'title', 'url'])
        writer.writeheader()
        for member in tar.getmembers():
            contents = tar.extractfile(member)
            if contents:
                resp = json.loads(contents.read())
                for item in resp:
                    if item['status_code'] == 200:
                        sub_data = json.loads(item['response'])
                        for activity in sub_data['activity']:
                            writer.writerow({
                                'list_id': sub_data['list_id'],
                                'email_id': sub_data['email_id'],
                                **activity
                            })
    temp_path.unlink()
    return outpath


def batch_still_pending(batch_response):
    if batch_response['status'] == 'finished':
        return False
    else:
        return True

def _retry_get_batch_status(client, batch_id, retries=5, delay=5):
    """Can't use requests.Session retry mechanism because
    the underlying client doesn't use it"""
    try:
        return client.batches.get(batch_id)
    except ConnectionError as err:
        if retries <= 0:
            raise
        else:
            print(err)
            print("Retrying attempt", retries)
            time.sleep(delay)
            return _retry_get_batch_status(client=client,
                                           batch_id=batch_id,
                                           retries=retries-1,
                                           delay=delay)


def wait_for_batch_to_finish(client, batch_id, api_delay=BATCH_POLLING_DELAY):
    batch_status = _retry_get_batch_status(client, batch_id)
    print("Waiting for batch operation {} to finish".format(batch_id))
    while batch_still_pending(batch_status):
        batch_status = _retry_get_batch_status(client, batch_id)
        time.sleep(api_delay)
    else:
        print("Batch {} finished.\n"
              "total_operations: {}\n"
              "erorred_opeartions: {}\n"
              "finished_opeartions: {}".format(
              batch_status['id'],
              batch_status['total_operations'],
              batch_status['errored_operations'],
              batch_status['finished_operations']))
        return batch_status

def download_all_campaign_ids(client, list_id, what, since_send_time=None):
    # the argument cant be None so here (otherwise it returns None all the time
    # so) we hack it this way
    if since_send_time is None:
        kwargs = {}
    else:
        kwargs = {"since_send_time": since_send_time}

    ids = []
    if 'campaigns' in what:
        print("Getting campaign ids for list_id", list_id)
        campaigns = client.campaigns.all(list_id=list_id,
                                         fields='campaigns.id,campaigns.emails_sent',
                                         get_all=True,
                                         **kwargs)
        ids.extend([c['id'] for c in campaigns['campaigns'] if c['emails_sent'] > 0])
    if 'automations' in what:
        print("Getting automation ids for list_id", list_id)
        automations = client.automations.all(list_id=list_id,
                                             fields='automations.id,automations.emails_sent',
                                             get_all=True)
        for automation in automations['automations']:
            emails = client.automations.emails.all(automation['id'], fields='emails.id,emails.emails_sent,emails.settings.title')
            ids.extend([a['id'] for a in emails['emails'] if a['emails_sent'] > 0])

    # We filter for nonzero campaigns otherwise the api returns empty response...
    return ids


def download_subscriber_activity_for_list_of_campaigns(client, all_campaigns, since, include_empty):
    if not all_campaigns:
        print("No campaigns for given configuration (since={})".format(since))
    activity_columns = ["campaign_id", "email_address", "timestamp",
                        "action", "ip", "url"]
    tmp_dir = '/tmp/mcex/subscriber_activity'
    if not os.path.isdir(tmp_dir):
        os.makedirs(tmp_dir)
    activity_chunks = []
    for campaign in all_campaigns:
        tmp_path = os.path.join(tmp_dir, campaign + '.csv')
        outpath = client.subscriber_activity(
            campaign,
            outpath=tmp_path,
            since=since,
            include_empty=include_empty)
        activity_chunks.append(outpath)
    activity_outpath = os.path.join(os.getenv("KBC_DATADIR"), 'out/tables', 'subscriber_activity.csv')

    # merge all tmp csvs to one big
    with open(activity_outpath, 'w') as out:
        outwriter = csv.DictWriter(out, activity_columns)
        outwriter.writeheader()
        for chunk in activity_chunks:
            with open(chunk, 'r') as f:
                reader = csv.DictReader(f)
                for line in reader:
                    outwriter.writerow(line)
    return activity_outpath


def arguments_combinations(list_config):
    """
    Given this structure:
    {
        "list_id": "e940b9878a",
        "status": ["unsubscribed", "cleaned"],
        "segment": ['foo', 'bar']
    }

    yield all permutations of
    e940b9878a, unsubscribed, foo
    e940b9878a, unsubscribed, bar
    e940b9878a, cleaned, foo
    e940b9878a, cleaned, bar
    in the form of Namedtuple

    in case segments is None or empty
    """

    # segments can be either empty list or not defined at all
    # so we handle both cases here
    segments = list_config.get('segments') or [None]
    list_id = list_config.get('list_id')
    status = list_config.get('status')
    _namedtuple_args_str = "list_id status segment"

    ListDownloadArgs = namedtuple("Args", _namedtuple_args_str)
    for combo in itertools.product([list_id], status, segments):
        yield ListDownloadArgs(*combo)


def parse_statefile(path):
    print("Reading statefile")
    try:
        with open(path) as f:
            state = json.load(f)
    except FileNotFoundError:
        print("Expected statefile at %s but didn't find any.", path)
        state = {}
    else:
        return state

def update_statefile(new_state, path='/data/out/state.json'):
    print("Updating statefile")
    with open(path, 'w') as f:
        json.dump(new_state, f)
